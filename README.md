# Hypoxicator-hardware
# STM32F401RET6 LQFP-64
# PINOUT: 
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
# ЛЕВАЯ КОЛОННА      - PB8
# МЕЖДУ КОЛОННАМИ    - PC5
# ПРАВАЯ КОЛОННА     - PB9
# -
# N2 ПОТРЕБИТЕЛЮ     - PA6
# N2 СБРОС           - PA7 
# N2 К КОМПРЕССОРУ   - PC8
# -
# O2 ПОТРЕБИТЕЛЮ     - PC0
# O2 СБРОС           - PA5
# КЛАПАН КОМПРЕССОРА - PC6
# РЕЛЕ КОМПРЕССОРА   - PC9
# ВЕНТИЛЯТОР         - PC10

# DMA для USART:
# USART1 - SpO2
# USART2 - Main
# USART6 - O2